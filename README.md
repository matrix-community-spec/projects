# Projects

List of projects in this group, as well as an issue tracker to request new projects.

## Making a request.

To request a new Project or Group, create an issue with one of the templates below

 * [Request Project](https://gitlab.com/matrix-community-spec/projects/-/issues/new?issuable_template=Project-Request)
 * [Request Group](https://gitlab.com/matrix-community-spec/projects/-/issues/new?issuable_template=Group-Request)
